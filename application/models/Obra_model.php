<?php

class Obra_model extends CI_Model
{

    public function select()
    {
        $sql = "select * from obras order by titulo asc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function selectLimit()
    {
        $sql = "select * from obras order by titulo asc limit 6";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function find($slug)
    {
        $sql = "select * from obras where slug = ?";
        $query = $this->db->query($sql, $slug);
        return $query->row();
    }
}
