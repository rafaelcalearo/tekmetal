<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

   <title>Tekmetal Engenharia</title>

   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
   <link rel="icon" href="images/favicon.ico" type="image/x-icon">

   <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/responsive.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">
   <!--<link rel="stylesheet" href="?= base_url('assets/css/animate.css') ?>"> -->
   <link rel="stylesheet" href="<?= base_url('assets/css/owl.carousel.min.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/owl.theme.default.min.css') ?>">
   <!--<link rel="stylesheet" href="?= base_url('assets/css/colorbox.css') ?>">-->
   <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
   <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
   <div class="body-inner">

      <?php
      $data['n'] = 2;
      $data['sobre'] = '';
      $this->load->view('components/header', $data);
      ?>

      <div id="banner-area" class="banner-area banner-empresa">
         <div class="banner-text">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="banner-heading">
                        <h1 class="banner-title">SERVIÇOS</h1>
                        <h6 style="color: #fff">A TEKMETAL ENG. OFERECE SOLUÇÕES PERSONALIZADAS PARA CADA CLIENTE.</h6>
                        <ol class="breadcrumb">
                           <li><a href="<?= base_url() ?>">HOME</a></li>
                           <li><a href="<?= base_url('servicos') ?>">SERVIÇOS</a></li>
                           <li><?= $servico->titulo ?></li>
                        </ol>
                     </div>
                  </div><!-- Col end -->
               </div><!-- Row end -->
            </div><!-- Container end -->
         </div><!-- Banner text end -->
      </div><!-- Banner area end -->

      <section id="main-container" class="main-container">
         <div class="container">
            <div class="row">

               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                  <div class="post-content post-single">
                     <div class="post-media post-image image-angle">
                        <img src="<?= base_url('assets/img/news/' . $servico->foto) ?>" class="img-responsive" alt="<?= $servico->titulo ?>">
                     </div>

                     <div class="post-body">
                        <div class="entry-header">
                           <div class="post-meta">
                              <span class="post-comment">
                                 <i class="fa fa-calendar"></i> <?= date("d/m/Y H:m:s", strtotime($servico->data)) ?>
                              </span>
                           </div>
                           <h2 class="entry-title">
                              <?= $servico->titulo ?>
                           </h2>
                        </div>

                        <div class="entry-content">
                           <?= $servico->descricao ?>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <?php $this->load->view('components/footer'); ?>

      <!-- initialize jQuery Library -->
      <script type="text/javascript" src="<?= base_url('assets/js/jquery.js') ?>"></script>
      <!-- Bootstrap jQuery -->
      <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
      <!-- Owl Carousel -->
      <script type="text/javascript" src="<?= base_url('assets/js/owl.carousel.min.js') ?>"></script>
      <!-- Color box 
		<script type="text/javascript" src="?= base_url('assets/js/jquery.colorbox.js') ?>"></script>-->
      <!-- Isotope 
		<script type="text/javascript" src="?= base_url('assets/js/isotope.js') ?>"></script>
		<script type="text/javascript" src="?= base_url('assets/js/ini.isotope.js') ?>"></script>
		-->

      <!-- Google Map API Key
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
		-->
      <!-- Google Map Plugin
		<script type="text/javascript" src="?= base_url('assets/js/gmap3.js') ?>"></script>
		-->
      <!-- Template custom -->
      <script type="text/javascript" src="<?= base_url('assets/js/custom.js') ?>"></script>
   </div><!-- Body inner end (fim) -->
</body>

</html>