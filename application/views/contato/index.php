<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

   <title>TEKMETAL ENG. | Contato</title>

   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
   <link rel="icon" href="images/favicon.ico" type="image/x-icon">

   <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/responsive.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/owl.carousel.min.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/owl.theme.default.min.css') ?>">
   <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
   <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

   <div class="body-inner">

      <?php
      $data['n'] = 5;
      $data['sobre'] = '';
      $this->load->view('components/header', $data);
      ?>

      <div id="banner-area" class="banner-area banner-empresa">
         <div class="banner-text">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="banner-heading">
                        <h1 class="banner-title">CONTATO</h1>
                        <h6 style="color: #fff">A TEKMETAL ENG. OFERECE SOLUÇÕES PERSONALIZADAS PARA CADA CLIENTE.</h6>
                        <ol class="breadcrumb">
                           <li><a href="<?= base_url() ?>">HOME</a></li>
                           <li>CONTATO</li>
                        </ol>
                     </div>
                  </div><!-- Col end -->
               </div><!-- Row end -->
            </div><!-- Container end -->
         </div><!-- Banner text end -->
      </div><!-- Banner area end -->

      <section id="main-container" class="main-container">
         <div class="container">

            <div class="row text-center">
               <h2 class="section-title">TODOS OS CONTATOS</h2>
               <h3 class="section-sub-title">SAIBA MAIS</h3>
            </div>
            <!--/ Title row end -->

            <div class="row">
               <div class="col-md-4">
                  <div class="ts-service-box-bg text-center">
                     <span class="ts-service-icon icon-round">
                        <i class="fa fa-map-marker"></i>
                     </span>
                     <div class="ts-service-box-content">
                        <h4>NOSSO LOCAL</h4>
                        <p>R. Pedro XX, n. 180 - Pelotas (RS)</p>
                     </div>
                  </div>
               </div><!-- Col 1 end -->

               <div class="col-md-4">
                  <div class="ts-service-box-bg text-center">
                     <span class="ts-service-icon icon-round">
                        <i class="fa fa-envelope"></i>
                     </span>
                     <div class="ts-service-box-content">
                        <h4>E-MAILS</h4>
                        <p>tekmetal.contato@gmail.com</p>
                     </div>
                  </div>
               </div><!-- Col 2 end -->

               <div class="col-md-4">
                  <div class="ts-service-box-bg text-center">
                     <span class="ts-service-icon icon-round">
                        <i class="fa fa-phone-square"></i>
                     </span>
                     <div class="ts-service-box-content">
                        <h4>TELEFONES</h4>
                        <p>(53) 99999-9999</p>
                     </div>
                  </div>
               </div><!-- Col 3 end -->

            </div><!-- 1st row end -->

            <!--
         <div class="gap-60"></div>

         <div id="map" class="map"></div>

         <div class="gap-40"></div>
         -->

            <br>

            <div class="row">

               <div class="col-md-12">

                  <h3 class="column-title">ENVIE SUA DÚVIDA OU SUGESTÃO</h3>

                  <form id="contact_form" method="post">
                     <div class="error-container"></div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="form-group">
                              <label>Nome completo</label>
                              <input class="form-control form-control-name" name="nome" id="nome" placeholder="" type="text">
                              <span class="text-danger" id="nome_error"></span>
                           </div>
                        </div>                       
                        <div class="col-md-4">
                           <div class="form-group">
                              <label>Celular</label>
                              <input class="form-control form-control-subject" name="celular" id="celular" placeholder="" type="text">
                              <span class="text-danger" id="celular_error"></span>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="form-group">
                              <label>Assunto</label>
                              <input class="form-control form-control-email" name="assunto" id="assunto" placeholder="" type="text">
                              <span class="text-danger" id="assunto_error"></span>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label>Mensagem</label>
                        <textarea class="form-control form-control-message" name="mensagem" id="mensagem" placeholder="" rows="10"></textarea>
                        <span class="text-danger" id="mensagem_error"></span>
                     </div>
                     <div id="success">
                     
                     </div>
                     <div class="text-right"><br>
                        <button class="btn btn-primary solid blank" type="submit">Enviar</button>
                     </div>
                  </form>
               </div>

            </div><!-- Content row -->
         </div><!-- Conatiner end -->
      </section><!-- Main container end -->

      <?php $this->load->view('components/footer'); ?>

      <!--<script type="text/javascript" src="?= base_url('assets/js/jquery.js') ?>"></script>-->
      <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
      <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
      <script type="text/javascript" src="<?= base_url('assets/js/owl.carousel.min.js') ?>"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
      <script type="text/javascript" src="<?= base_url('assets/js/custom.js') ?>"></script>
      <script>
         $(document).ready(function() {
            $('#celular').mask('(00) 00000-0000');
            $('#contact_form').on('submit', function(event) {
               event.preventDefault();
               $.ajax({
                  url: "<?php echo base_url(); ?>contato/validation",
                  method: "POST",
                  data: $(this).serialize(),
                  dataType: "json",
                  success: function(data) {
                     if (data.error) {
                        if (data.nome_error != '') {
                           $('#nome_error').html(data.nome_error);
                        } else {
                           $('#nome_error').html('');
                        }
                        if (data.celular_error != '') {
                           $('#celular_error').html(data.celular_error);
                        } else {
                           $('#celular_error').html('');
                        }
                        if (data.assunto_error != '') {
                           $('#assunto_error').html(data.assunto_error);
                        } else {
                           $('#assunto_error').html('');
                        }
                        if (data.mensagem_error != '') {
                           $('#mensagem_error').html(data.mensagem_error);
                        } else {
                           $('#mensagem_error').html('');
                        }
                     }
                     if (data.success) {
                        $('#success').html(data.success);
                        $('#nome_error').html('');
                        $('#celular_error').html('');
                        $('#assunto_error').html('');
                        $('#mensagem_error').html('');
                        $('#nome').val('');
                        $('#celular').val('');
                        $('#assunto').val('');
                        $('#mensagem').val('');
                     }
                  }
               })
            })
         });
      </script>
   </div><!-- Body inner end (fim) -->
</body>

</html>