<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<title>Tekmetal Engenharia</title>

	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">

	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/responsive.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">
	<!--<link rel="stylesheet" href="?= base_url('assets/css/animate.css') ?>">
	<link rel="stylesheet" href="?= base_url('assets/css/owl.carousel.min.css') ?>">
	<link rel="stylesheet" href="?= base_url('assets/css/owl.theme.default.min.css') ?>">
	<link rel="stylesheet" href="?= base_url('assets/css/colorbox.css') ?>">-->
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div class="body-inner">
		<?php
		$data['n'] = 0;
		$data['sobre'] = '';
		$this->load->view('components/header', $data);
		?>

		<div id="main-slide" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slide" data-slide-to="0" class="active"></li>
				<li data-target="#main-slide" data-slide-to="1"></li>
				<li data-target="#main-slide" data-slide-to="2"></li>
			</ol>

			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img style="width: 100%" src="assets/img/slider-main/slide-01.jpg" alt="...">
					<div class="carousel-caption animated6">
						<h1>PRÉ-MOLDADOS</h1>
						<p class="paragrafo">
							Além de ser prático, o sistema pré-moldado possui uma qualidade superior ao tradicional
							devido ao rigoroso controle durante todo o processo de fabricação. Empregamos alta
							tecnologia em nossa produção, proporcionando um material que se adéqua perfeitamente às
							necessidades da sua obra.
						</p>
						<p><a class="btn btn-lg btn-primary" href="<?= base_url('servicos/pre-moldados') ?>" role="button">SAIBA MAIS</a></p>
					</div>
				</div>
				<div class="item">
					<img style="width: 100%" src="assets/img/slider-main/slide-02.jpg" alt="...">
					<div class="carousel-caption animated6">
						<h1>TELHA COLONIAL</h1>
						<p class="paragrafo">
							A Telha Colonial da LP Cousen é uma telha em formato colonial, com linhas suaves e com um
							movimento que parecem as ondas do mar, dando um acabamento requintado e aconchegante à sua
							cobertura.
						</p>
						<p><a class="btn btn-lg btn-primary" href="<?= base_url('servicos/telha-colonial') ?>" role="button">SAIBA MAIS</a></p>
					</div>
				</div>
				<div class="item">
					<img style="width: 100%" src="assets/img/slider-main/slide-03.jpg" alt="...">
					<div class="carousel-caption animated6">
						<h1>PAINÉIS E FACHADAS EM ACM</h1>
						<p class="paragrafo">
							O ACM é dobrável é muito fácil de manipular. Ainda é flexível, leve e fácil de limpar, além
							de ser resistente ao desgaste do tempo. Usado em diversos projetos...
						</p>
						<p><a class="btn btn-lg btn-primary" href="<?= base_url('servicos/paineis-e-fachadas-em-acm') ?>" role="button">SAIBA MAIS</a></p>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#main-slide" data-slide="prev">
				<span><i class="fa fa-angle-left"></i></span>
			</a>
			<a class="right carousel-control" href="#main-slide" data-slide="next">
				<span><i class="fa fa-angle-right"></i></span>
			</a>
		</div>

		<section id="news" class="news">
			<div class="container">
				<div class="row text-center">
					<h2 class="section-title">TRABALHAMOS COM EXELÊNCIA</h2>
					<h3 class="section-sub-title">SERVIÇOS</h3>
				</div>

				<div class="row">
					<?php foreach ($servicos as $servico) { ?>
						<div class="col-md-4 col-xs-12 divisor">
							<div class="latest-post">
								<div class="latest-post-media">
									<a href="<?= base_url() . 'servicos/' . $servico->slug ?>" class="latest-post-img">
										<?php echo '<img class="img-responsive" src="assets/img/news/' . $servico->foto . '" alt="' . $servico->titulo . '">'; ?>
									</a>
								</div>
								<div class="post-body">
									<h4 class="post-title">
										<a href="<?= base_url() . 'servicos/' . $servico->slug ?>">
											<?= $servico->titulo ?>
										</a>
									</h4>
									<div class="latest-post-meta">
										<span class="post-item-date">
											<a href="<?= base_url() . 'servicos/' . $servico->slug ?>">Saiba mais</a>
										</span>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>					
				</div>				

				<div class="general-btn text-center">
					<a href="<?= base_url('servicos') ?>" class="btn btn-primary">TODOS OS SERVIÇOS</a>
				</div>

			</div>
		</section>

		<section class="subscribe no-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="subscribe-call-to-acton">
							<h3>FICOU COM DÚVIDAS?</h3>
							<h4>(53) 98123-0000</h4>
						</div>
					</div>

					<div class="col-md-8 col-sm-12 col-xs-12">
						<div class="ts-newsletter">
							<div class="newsletter-introtext">
								<h4>NEWSLETTER</h4>
								<p>Atualizações e notícias</p>
							</div>

							<div class="newsletter-form">
								<form action="#" method="post">
									<div class="form-group">
										<input type="email" name="email" id="newsletter-form-email" class="form-control form-control-lg" placeholder="Seu melhor e-mail" autocomplete="off">
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>

		<?php $this->load->view('components/footer'); ?>

		<!-- initialize jQuery Library -->
		<script type="text/javascript" src="<?= base_url('assets/js/jquery.js') ?>"></script>
		<!-- Bootstrap jQuery -->
		<script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="<?= base_url('assets/js/owl.carousel.min.js') ?>"></script>
		<!-- Color box 
		<script type="text/javascript" src="?= base_url('assets/js/jquery.colorbox.js') ?>"></script>-->
		<!-- Isotope 
		<script type="text/javascript" src="?= base_url('assets/js/isotope.js') ?>"></script>
		<script type="text/javascript" src="?= base_url('assets/js/ini.isotope.js') ?>"></script>
		-->

		<!-- Google Map API Key
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
		-->
		<!-- Google Map Plugin
		<script type="text/javascript" src="?= base_url('assets/js/gmap3.js') ?>"></script>
		-->
		<!-- Template custom -->
		<script type="text/javascript" src="<?= base_url('assets/js/custom.js') ?>"></script>
	</div><!-- Body inner end (fim) -->
</body>

</html>