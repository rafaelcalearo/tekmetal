<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

   <title>Tekmetal Engenharia</title>

   <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
   <link rel="icon" href="images/favicon.ico" type="image/x-icon">

   <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/responsive.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/font-awesome.min.css') ?>">
   <!--<link rel="stylesheet" href="?= base_url('assets/css/animate.css') ?>"> -->
   <link rel="stylesheet" href="<?= base_url('assets/css/owl.carousel.min.css') ?>">
   <link rel="stylesheet" href="<?= base_url('assets/css/owl.theme.default.min.css') ?>">
   <!--<link rel="stylesheet" href="?= base_url('assets/css/colorbox.css') ?>">-->
   <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
   <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

   <div class="body-inner">

      <?php
      $data['n'] = 1;
      $data['sobre'] = 'empresa';
      $this->load->view('components/header', $data);
      ?>

      <div id="banner-area" class="banner-area banner-empresa">
         <div class="banner-text">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="banner-heading">
                        <h1 class="banner-title">A EMPRESA</h1>
                        <h6 style="color: #fff">A TEKMETAL ENG. OFERECE SOLUÇÕES PERSONALIZADAS PARA CADA CLIENTE.</h6>
                        <ol class="breadcrumb">
                           <li><a href="<?= base_url() ?>">HOME</a></li>
                           <li>EMPRESA</li>
                        </ol>                        
                     </div>
                  </div><!-- Col end -->
               </div><!-- Row end -->
            </div><!-- Container end -->
         </div><!-- Banner text end -->
      </div><!-- Banner area end -->


      <section id="main-container" class="main-container">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <h3 class="column-title">SOBRE A EMPRESA</h3>
                  <p>A <strong>Metalúrgica TEKMETAL</strong> se consolidou no mercado pela
                     sua qualificação e qualidade dos produtos e serviços executados em obras
                     de empresas das áreas agrícolas, comerciais e de serviços.</p>
                  <p>O reconhecimento e a indicação de seus clientes se tornou uma fonte de
                     prospecção para novos trabalhos. Com uma equipe própria treinada para
                     operar equipamentos de última geração fazem da <strong>TEKMETAL</strong> uma empresa
                     diferenciada, possibilitando flexibilidade, precisão, agilidade, maior
                     custo benefício e cumprimento nos prazos de entrega de suas obras.</p>
                  <p>A <strong>Metalúrgica TEKMETAL</strong> trabalha com projetos e execução de
                     <strong>COBERTURAS METÁLICAS</strong>, <strong>METÁLICAS</strong>, <strong>PRÉ-MOLDADOS</strong>,
                     <strong>CERCAS INDUSTRIAIS</strong>, <strong>CORTINAS METÁLICAS INDUSTRIAIS</strong>, <strong>MEZANINOS</strong>,
                     <strong>PORTÕES</strong> e <strong>ESCADAS</strong> com uma redução de custos, resultado de sua
                     tecnologia de processamento e utilização de equipamentos como gruas e
                     guindastes telescópios que resultam em menor custo x benefícios aos seus
                     clientes.</p>
                  <p>Na área de <strong>pré-moldados</strong> a parceria com empresa de mesma filosofia e 
                     qualidade de trabalho complementam a demanda do mercado em todos os tipos 
                     de empreendimentos.</p><br />
                  <p><a class="btn btn-primary" href="<?= base_url('qualidade') ?>">QUALIDADE</a></p>
               </div><!-- Col end -->

               <div class="col-md-6">

                  <div id="page-slider" class="owl-carousel owl-theme page-slider small-bg">

                     <div class="item" style="background-image:url(assets/img/slider-pages/slide-page1.jpg); background-size: cover">
                        <div class="container">
                           <div class="box-slider-content">
                              <div class="box-slider-text">
                                 <h2 class="box-slide-title">ESTRUTURAS METÁLICAS</h2>
                              </div>
                           </div>
                        </div>
                     </div><!-- Item 1 end -->

                     <div class="item" style="background-image:url(assets/img/slider-pages/slide-page2.jpg); background-size: cover">
                        <div class="container">
                           <div class="box-slider-content">
                              <div class="box-slider-text">
                                 <h2 class="box-slide-title">PAINÉIS E FACHADAS EM ACM</h2>
                              </div>
                           </div>
                        </div>
                     </div><!-- Item 1 end -->

                     <div class="item" style="background-image:url(assets/img/slider-pages/slide-page3.jpg); background-size: cover">
                        <div class="container">
                           <div class="box-slider-content">
                              <div class="box-slider-text">
                                 <h2 class="box-slide-title">PRÉ MOLDADOS</h2>
                              </div>
                           </div>
                        </div>
                     </div><!-- Item 1 end -->
                  </div><!-- Page slider end-->


               </div><!-- Col end -->
            </div><!-- Content row end -->

         </div><!-- Container end -->
      </section><!-- Main container end -->

      <?php $this->load->view('components/footer'); ?>

      <!-- initialize jQuery Library -->
      <script type="text/javascript" src="<?= base_url('assets/js/jquery.js') ?>"></script>
      <!-- Bootstrap jQuery -->
      <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
      <!-- Owl Carousel -->
      <script type="text/javascript" src="<?= base_url('assets/js/owl.carousel.min.js') ?>"></script>
      <!-- Color box 
		<script type="text/javascript" src="?= base_url('assets/js/jquery.colorbox.js') ?>"></script>-->
      <!-- Isotope 
		<script type="text/javascript" src="?= base_url('assets/js/isotope.js') ?>"></script>
		<script type="text/javascript" src="?= base_url('assets/js/ini.isotope.js') ?>"></script>
		-->

      <!-- Google Map API Key
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
		-->
      <!-- Google Map Plugin
		<script type="text/javascript" src="?= base_url('assets/js/gmap3.js') ?>"></script>
		-->
      <!-- Template custom -->
      <script type="text/javascript" src="<?= base_url('assets/js/custom.js') ?>"></script>
   </div><!-- Body inner end (fim) -->
</body>

</html>