<div id="top-bar" class="top-bar">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
				<ul class="top-info">
					<li>
						<i class="fa fa-map-marker"></i>
						<p class="info-text">Entre em contato conosco</p>
					</li>
				</ul>
			</div>
			<!--/ Top info end -->

			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 top-social text-right">
				<ul class="unstyled">
					<li>
						<a title="Facebook" href="#">
							<span class="social-icon"><i class="fa fa-facebook"></i></span>
						</a>
						<a title="Instagram" href="#">
							<span class="social-icon"><i class="fa fa-instagram"></i></span>
						</a>
					</li>
				</ul>
			</div>
			<!--/ Top social end -->
		</div>
		<!--/ Content row end -->
	</div>
	<!--/ Container end -->
</div>
<!--/ Topbar end -->

<!-- Header start -->
<header id="header" class="header-one">
	<div class="container">
		<div class="row">
			<div class="logo-area clearfix">
				<div class="logo col-xs-12 col-md-4">
					<a href="<?= base_url() ?>">
						<img src="<?= base_url('assets/img/logo.png') ?>" width="150" height="75">
					</a>
				</div><!-- logo end -->
				<div class="col-xs-12 col-md-8 header-right">
					<ul class="top-info-box">
						<li>
							<div class="info-box">
								<div class="info-box-content">
									<p class="info-box-title">TELEFONE</p>
									<p class="info-box-subtitle">(53) 3245-4353</p>
								</div>
							</div>
						</li>
						<li>
							<div class="info-box">
								<div class="info-box-content">
									<p class="info-box-title">WHATSAPP</p>
									<p class="info-box-subtitle">(53) 99979-5835</p>
								</div>
							</div>
						</li>
						<li class="last">
							<div class="info-box last">
								<div class="info-box-content">
									<p class="info-box-title">E-MAIL</p>
									<p class="info-box-subtitle">contato@tekmetal.eng.br</p>
								</div>
							</div>
						</li>
						<li class="header-get-a-quote">
							<a class="btn btn-primary" href="<?= base_url('contato') ?>">CONTATE-NOS</a>
						</li>
					</ul><!-- Ul end -->
				</div><!-- header right end -->
			</div><!-- logo area end -->

		</div><!-- Row end -->
	</div><!-- Container end -->

	<nav class="site-navigation navigation navdown">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="site-nav-inner pull-left">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<div class="collapse navbar-collapse navbar-responsive-collapse">
							<ul class="nav navbar-nav">
								<li class="dropdown <?= $n === 0 ? 'active' : '' ?>">
									<a href="<?= base_url() ?>" class="dropdown-toggle">HOME</a>
								</li>
								<li class="dropdown <?= $n === 1 ? 'active' : '' ?>">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">SOBRE <i class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu" role="menu">
										<li class="<?= $sobre === 'empresa' ? 'active' : '' ?>"><a href="<?= base_url('empresa') ?>">EMPRESA</a></li>
										<li class="<?= $sobre === 'qualidade' ? 'active' : '' ?>"><a href="<?= base_url('qualidade') ?>">QUALIDADE</a></li>
									</ul>
								</li>
								<li class="dropdown <?= $n === 2 ? 'active' : '' ?>">
									<a href="<?= base_url('servicos') ?>" class="dropdown-toggle">SERVIÇOS</a>
								</li>

								<li class="dropdown <?= $n === 3 ? 'active' : '' ?>">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">OBRAS</a>
								</li>

								<li class="dropdown <?= $n === 4 ? 'active' : '' ?>">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">NOTÍCIAS</a>
								</li>
								<li class="dropdown <?= $n === 5 ? 'active' : '' ?>">
									<a href="<?= base_url('contato') ?>">CONTATO</a>
								</li>
							</ul>
							<!--/ Nav ul end -->
						</div>
						<!--/ Collapse end -->

					</div><!-- Site Navbar inner end -->

				</div>
				<!--/ Col end -->
			</div>
			<!--/ Row end -->
			<!--
					<div class="nav-search">
						<span id="search"><i class="fa fa-search"></i></span>
					</div>-- Search end 

					<div class="search-block" style="display: none;">
						<input type="text" class="form-control" placeholder="Type what you want and enter">
						<span class="search-close">&times;</span>
					</div>Site search end -->
		</div>
		<!--/ Container end -->

	</nav>
	<!--/ Navigation end -->
</header>