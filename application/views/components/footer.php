<footer id="footer" class="footer bg-overlay">
			<div class="footer-main">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12 footer-widget footer-about">
							<h3 class="widget-title">A EMPRESA</h3>
							<!--<img class="footer-logo" src="images/logo.png" alt="" width="80" height="80" />-->
							<p>
								Fundada no ano de 0000 a Tekmetal Engenharia é uma empresa que atua em diversos
								seguimentos da metalurgia com foco no ramo corporativo, procurando atender de forma
								abrangente aos seus clientes dando suporte e acompanhamento nos serviços prestados.</p>
							<div class="footer-social">
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								</ul>
							</div><!-- Footer social end -->
						</div><!-- Col end -->

						<div class="col-md-4 col-sm-12 footer-widget">
							<h3 class="widget-title">JORNADA DE TRABALHO</h3>
							<div class="working-hours">
								Trabalhamos 7 dias por semana, todos os dias, excluindo os principais feriados. Entre em
								contato conosco em caso de dúvidas, pelo nosso formulário de contato ou chat.
								<br><br> Segunda-feira a sexta-feira: <span class="text-right">10:00 - 16:00 </span>
								<br> Sábado: <span class="text-right">12:00 - 15:00</span>
								<br> Domingos e feriados: <span class="text-right">09:00 - 12:00</span>
							</div>
						</div><!-- Col end -->

						<div class="col-md-4 col-sm-12 footer-widget">
							<h3 class="widget-title">SERVIÇOS</h3>
							<ul class="list-arrow">
								<li><a href="#">Pré-moldados</a></li>
								<li><a href="#">Paines e fachadas em ACM</a></li>
								<li><a href="#">Estruturas metálicas</a></li>
								<li><a href="#">Telhas coloniais</a></li>
								<li><a href="#">Reforma de máquinas</a></li>
							</ul>
						</div><!-- Col end -->


					</div><!-- Row end -->
				</div><!-- Container end -->
			</div><!-- Footer main end -->

			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="copyright-info">
								<span>Copyright © 2020 by <a href="#">RTO</a></span>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6">
							<div class="footer-menu">
								<ul class="nav unstyled">
									<li><a href="#">Sobre</a></li>
									<li><a href="#">Colaboradores</a></li>
									<li><a href="#">FAQ</a></li>									
									<li><a href="#">Preços</a></li>
								</ul>
							</div>
						</div>
					</div><!-- Row end -->

					<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
						<button class="btn btn-primary" title="VOLTAR AO TOPO?">
							<i class="fa fa-angle-double-up"></i>
						</button>
					</div>

				</div><!-- Container end -->
			</div><!-- Copyright end -->

		</footer><!-- Footer end -->