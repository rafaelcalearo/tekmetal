<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Obras extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$this->load->model('Obras_model', 'obras');
	}

	public function index()
	{
		$dados['obras'] = $this->obras->select();
		$this->load->view('obras/index', $dados);
	}

	public function view($slug)
	{
		$dados['obra'] = $this->obras->find($slug);
		$this->load->view('obras/view', $dados);
	}
}
