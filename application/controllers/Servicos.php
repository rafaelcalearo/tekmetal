<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Servicos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$this->load->model('Servico_model', 'servico');
	}

	public function index()
	{
		$dados['servicos'] = $this->servico->select();
		$this->load->view('servicos/index', $dados);
	}

	public function view($slug)
	{
		$dados['servico'] = $this->servico->find($slug);
		$this->load->view('servicos/view', $dados);
	}
}
