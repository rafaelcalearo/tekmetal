<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contato extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view('contato/index');
	}

	public function validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules(
			'nome',
			'Nome',
			'required',
			array('required' => 'O campo é obrigatório!')
		);
		$this->form_validation->set_rules(
			'celular',
			'Celular',
			'required',
			array('required' => 'O campo é obrigatório!')
		);
		$this->form_validation->set_rules(
			'assunto',
			'Assunto',
			'required',
			array(
				'required' => 'O campo é obrigatório!'
			)
		);
		$this->form_validation->set_rules(
			'mensagem',
			'Mensagem',
			'required',
			array('required' => 'O campo é obrigatório!')
		);
		$this->form_validation->set_error_delimiters(
			'<ul role="alert"><li>',
			'</li></ul>'
		);

		if ($this->form_validation->run()) {
			$enviaFormularioParaNome = 'Contato';
			$enviaFormularioParaEmail = 'tekmetal.contato@gmail.com';
			$caixaPostalServidorNome = 'TEKMETAL ENG. | Formulário de contato';
			$caixaPostalServidorEmail = 'dispara@tekmetal.eng.br';
			$caixaPostalServidorSenha = 'TEKMETAL@12345e';

			$remetenteNome  = $this->input->post('nome');
			$remetenteCelular = $this->input->post('celular');
			$assunto  = $this->input->post('assunto');
			$mensagem = $this->input->post('mensagem');
			$mensagemConcatenada = 'Formulário gerado via website' . '<br/>';
			$mensagemConcatenada .= '-------------------------------<br/><br/>';
			$mensagemConcatenada .= 'Nome: ' . $remetenteNome . '<br/>';
			$mensagemConcatenada .= 'Celular: ' . $remetenteCelular . '<br/>';
			$mensagemConcatenada .= 'Assunto: ' . $assunto . '<br/>';
			$mensagemConcatenada .= '-------------------------------<br/><br/>';
			$mensagemConcatenada .= 'Mensagem: "' . $mensagem . '"<br/>';

			require('PHPMailer_5.2.4/class.phpmailer.php');

			$mail = new PHPMailer();

			$mail->IsSMTP();
			$mail->SMTPAuth = true;
			$mail->Charset = 'utf8_decode()';
			$mail->Host = 'smtp.umbler.com';
			$mail->Port = '587';
			$mail->Username = $caixaPostalServidorEmail;
			$mail->Password = $caixaPostalServidorSenha;
			$mail->From = $caixaPostalServidorEmail;
			$mail->FromName = utf8_decode($caixaPostalServidorNome);
			$mail->IsHTML(true);
			$mail->Subject = utf8_decode($assunto);
			$mail->Body = utf8_decode($mensagemConcatenada);
			$mail->AddAddress($enviaFormularioParaEmail, utf8_decode($enviaFormularioParaNome));

			if (!$mail->Send()) {
				print($mail->ErrorInfo);
				$array = array(
					'success' => '<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>Não foi possível enviar a mensagem!</strong></div>'
				);
			} else {
				$array = array(
					'success' => '<div class="alert alert-success alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span></button>
				<strong>Obrigado por fazer contato. Retornaremos!</strong>
				</div>'
				);
			}
		} else {
			$array = array(
				'error' => true,
				'nome_error' => form_error('nome'),
				'celular_error' => form_error('celular'),
				'assunto_error' => form_error('assunto'),
				'mensagem_error' => form_error('mensagem')
			);
		}
		echo json_encode($array);
	}
}
