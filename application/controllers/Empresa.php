<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Empresa extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		//$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view('empresa/empresa');
	}
}
